package ru.semenov.mapper;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import ru.semenov.exception.UserNotFoundException;
@Provider
public class UserNotFoundExceptionMapper implements ExceptionMapper<UserNotFoundException> {
    @Override
    public Response toResponse(UserNotFoundException exception) {
        return Response
                .status(Response.Status.NOT_FOUND.getStatusCode(),
                        "Пользователь не найден!!!")
                .build();
    }
}
