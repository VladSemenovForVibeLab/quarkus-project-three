package ru.semenov.builder;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import ru.semenov.entity.UserEntity;

@FieldDefaults(level = AccessLevel.PUBLIC)
public class UserEntityBuilder {
    String username;
    private UserEntityBuilder(){}
    public static UserEntityBuilder builder(){
        return new UserEntityBuilder();
    }
    public UserEntityBuilder setUsername(String username){
        this.username = username;
        return this;
    }
    public UserEntity build(){
        return new UserEntity(username);
    }
}
