package ru.semenov.controller;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import ru.semenov.entity.UserEntity;
import ru.semenov.service.UserService;

import java.util.UUID;

@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class UserController {
    UserService userService;
    @GET
    public Response test(){
        return Response.ok("test").build();
    }
    @POST
    public Response createUser(UserEntity userEntity){
        return Response
                .ok(userService.createUser(userEntity))
                .build();
    }
    @GET
    @Path("/findAll")
    public Response findAll(@QueryParam("page") @DefaultValue("0") Integer page,
                            @QueryParam("pageSize") @DefaultValue("10") Integer pageSize){
       var users = userService.findAll(page, pageSize);
       return Response
               .ok(users)
               .build();
    }
    @GET
    @Path("/{id}")
    public Response findById(@PathParam("id") UUID id){
        return Response.ok(userService.findById(id)).build();
    }
    @PUT
    @Path("/{id}")
    public Response update(@PathParam("id") UUID id,
                           UserEntity userEntity){
        return Response.ok(userService.update(id, userEntity)).build();
    }
    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") UUID id){
        userService.delete(id);
        return Response.ok().build();
    }
}
