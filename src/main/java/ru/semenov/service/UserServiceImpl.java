package ru.semenov.service;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import ru.semenov.entity.UserEntity;
import ru.semenov.exception.UserNotFoundException;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@ApplicationScoped
public class UserServiceImpl implements UserService{
    @Override
    @Transactional
    public UserEntity createUser(UserEntity userEntity) {
        UserEntity.persist(userEntity);
        return userEntity;
    }

    @Override
    public List<UserEntity> findAll(Integer page, Integer pageSize) {
        return UserEntity.findAll()
                .page(page,pageSize)
                .list();
    }

    @Override
    public UserEntity findById(UUID id) {
        return (UserEntity) UserEntity.findByIdOptional(id)
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    @Transactional
    public UserEntity update(UUID id, UserEntity userEntity) {
        var user = findById(id);
        user.username = userEntity.username;
        UserEntity.persist(user);
        return user;
    }

    @Override
    @Transactional
    public void delete(UUID id) {
        var user = findById(id);
        UserEntity.deleteById(user.userId);
    }


}
