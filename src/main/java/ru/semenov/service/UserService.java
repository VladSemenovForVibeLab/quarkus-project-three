package ru.semenov.service;

import ru.semenov.entity.UserEntity;

import java.util.List;
import java.util.UUID;

public interface UserService {
    UserEntity createUser(UserEntity userEntity);

    List<UserEntity> findAll(Integer page, Integer pageSize);

    UserEntity findById(UUID id);

    UserEntity update(UUID id, UserEntity userEntity);

    void delete(UUID id);
}
